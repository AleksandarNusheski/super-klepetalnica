# Opis naloge in navodila

Na BitBucket je na voljo javni repozitorij https://bitbucket.org/asistent/super-klepetalnica, ki vsebuje Node.js spletno klepetalnico predstavljeno na predavanjih in vajah. V okviru domače naloge ustvarite lastno kopijo repozitorija ter dopolnite obstoječo implementacijo spletne klepetalnice kot zahtevajo navodila. Domača naloga zahteva poznavanje GIT ukazov, HTML označb, CSS stilov, jezika JavaScript in Node.js tehnologije. Domača naloga ne zahteva veliko dela, a zahteva nekaj razmisleka. Pri delu natančno sledite navodilom!

### Vzpostavitev okolja in repozitorija

Na BitBucket z uporabo funkcije Fork ustvarite lastno kopijo repozitorija https://bitbucket.org/asistent/super-klepetalnica v okviru katere boste opravljali domačo nalogo. Kopija repozitorija mora biti zasebna in mora ohraniti ime “super-klepetalnica”. Vašemu repozitoriju dodajte uporabnika asistent in dlavbic z administratorskimi pravicami. Z uporabo podanih GIT ukazov v ukazni lupini razvojnega okolja Cloud9 vzpostavite lokalni repozitorij ter ustvarite veje razvoja krcanje, barva, slike in preimenovanje v okviru katerih boste reševali posamezne naloge.

### Krcanje

Spletna klepetalnica omogoča pošiljanje sporočil, vendar včasih bi želeli kakega od uporabnikov le krcniti. Zaradi tega v veji krcanje implementirajte, da se od kliku na vzdevek uporabnika le temu preko zasebnega sporočila pošlje HTML simbol ☜ (&#9756;). V oknu za sporočila se mora simbol tudi ustrezno prikazati v vseh primerih. (hint: Če sporočilo vsebuje HTML simbol, ga prikažite kot HTML vsebino.)

### Spreminjanje barve klepetalnice

Spletna klepetalnica uporablja rumeno za privzeto barvo ozadja. V veji barva dopolnite implementacijo tako, da bo lahko uporabnik z ukazom /barva {barva} spremenil barvo okna za sporočila in barvo zgornjega barvnega pasu. Primer: Uporabnik vnese ukaz /barva lightblue in ta ukaz vpliva le na njegovo okno.

Na začetno stran, kjer so vsi ukazi klepetalnice dokumentirani, pod ukazi za klepet, dodajte tudi tega.

### Deljenje slikovnih datotek

V veji slike dopolnite implementacijo spletne klepetalnice tako, da bo omogočala deljenje slikovnih datotek dostopnih preko spleta. V ta namen po vnosu sporočila uporabnika preverite ali le-to vsebuje povezave na slikovne datoteke. Predpostavite, da so to povezave (besede v sporočilu), ki se začnejo z nizom http in končajo z nizom .jpg, .png ali .gif. V kolikor take povezave najdete v sporočilu, na koncu sporočila dodajte ustrezno število HTML elementov <img>, ki bodo slike tudi dejansko prikazali. Uporabite atribut style elementa <img>, da nastavite širino prikazanih slik na 200px in odmik od levega robu na 20px. Vsaka slika naj bo prikazana v novi vrstici, za kar uporabite HTML element <br />. Pazite, da bodo slike prikazane tako pošiljatelju kot tudi prejemnikom. (hint: Besede so ločene s presledki. Lahko se pojavijo na začetku ali koncu niza. Zaključene so lahko tudi z ločilom.)

### Preimenovanje uporabnikov

V spletni klepetalnici si lahko vsak uporabnik nastavi poljubno uporabniško ime, ki ga vidijo drugi. Ker bi si uporabniki želeli nastavljati lastne zabeležke oz. nadimke drugim uporabnikom, v veji preimenovanje implementirajte ukaz /preimenuj "{uporabnik}" "{nadimek}". Uporabniku, ki je ukaz vnesel, naj se drugi uporabnik v seznamu prijavljenih uporabnikov na desni pokaže v obliki {nadimek} ({uporabnik}). Prav tako naj se takšen izpis uporablja v njegovem oknu za sporočila. Spremembe pa so veljavne le za uporabnika, ki je ukaz vnesel in veljajo od vnosa ukaza dalje. (hint: Bodite pozorni tudi na pošiljanje zasebnih sporočil in spremembe vzdevkov uporabnikov.)

Na začetno stran, kjer so vsi ukazi klepetalnice dokumentirani, pod ukazi za klepet, dodajte tudi tega.

### Združevanje sprememb v repozitoriju

Z uporabo GIT ukazov veje razvoja krcanje, barva, slike in preimenovanje združite v vejo master. Pri tem pazite, da samih vej ne izbrišete. Odpravite morebitne konflikte in spremembe uveljavite tudi v oddaljenem repozitoriju. (hint: Bodite pozorni, da po združitvi delujejo vse funkcionalnosti.)