/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeKrcenjeSmeskoAliSlika = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1 || sporocilo.indexOf("&#9756;") > -1 ||
    sporocilo.indexOf(".jpg") > -1 || sporocilo.indexOf(".png") > -1 || sporocilo.indexOf(".gif") > -1;
  if(jeKrcenjeSmeskoAliSlika){
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;br /&gt;&lt;img src=").join("<br /><img src=")
               .split(" style=\"width:200px;padding-left:20px;\" /&gt;").join(" style=\"width:200px;padding-left:20px;\" />")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    sporocilo = dodajSlike(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


/**
 * Dodaja HTML oznake za prikazovanje slike
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo slike
 */
function dodajSlike(sporocilo){
  var result=sporocilo.match(/http[^ ]*\.(gif|jpg|png)/g);
  if(result!=null)
    for(var i=0; i<result.length; i++)
      if(result[i].indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") == -1)
      	sporocilo+='<br /><img src="'+result[i]+'" style="width:200px;padding-left:20px;" />';
  return sporocilo;
}

var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Posodobi spremembe vzdevka v nadimek
  socket.on('vdevekNadimek', function(rezultat) {
    klepetApp.nadimek[rezultat.novVzdevek]=klepetApp.nadimek[rezultat.prejsnjiVzdevek];
    delete klepetApp.nadimek[rezultat.prejsnjiVzdevek];
    socket.emit('uporabniki', {kanal: trenutniKanal});
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    // Dodaj nadimek v sporočilo, če obstaja za podani vzdevek
    var text=sporocilo.besedilo;
    if(sporocilo.zasebno){
      text=sporocilo.besedilo.split(" (zasebno): ");
      if(text[0] in klepetApp.nadimek){
        text[0]=klepetApp.nadimek[text[0]] + " (" + text[0] + ")";
      }
      text=text.join(" (zasebno): ");
    }else{
      text=sporocilo.besedilo.split(": ");
      if(text[0] in klepetApp.nadimek){
        text[0]=klepetApp.nadimek[text[0]] + " (" + text[0] + ")";
      }
      text=text.join(": ");
    }
    var novElement = divElementEnostavniTekst(text);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    // Dodaj nadimek če obstaja za podani vzdevek
    for (var i=0; i < uporabniki.length; i++) {
      if(uporabniki[i] in klepetApp.nadimek){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(klepetApp.nadimek[uporabniki[i]] + " (" + uporabniki[i] + ")"));
      }else{
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }
    
    // Klik na vzdevek uporabnika v seznamu uporabnikov pošilja krcanje
    $('#seznam-uporabnikov div').click(function() {
      var tmp=$(this).text().split(" (");
      if(tmp.length>1) tmp=tmp[1].split(")")[0];
      var sistemskoSporocilo = klepetApp.procesirajUkaz('/zasebno \"' + tmp + '\" \"&#9756;\"');
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
