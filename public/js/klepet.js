// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika

var Klepet = function(socket) {
  this.socket = socket;
  this.nadimek = {};
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        parametri[3]=dodajSlike(parametri[3]);
        this.socket.emit('sporocilo', {
          vzdevek: parametri[1], besedilo: parametri[3]
        });
        sporocilo = '(zasebno za ' + (parametri[1] in this.nadimek ?
          this.nadimek[parametri[1]] + " (" + parametri[1] + ")" : parametri[1]) + '): ' + parametri[3];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'barva':
      var trenutnaBarva=document.getElementById("sporocila").style.backgroundColor;
      $('#kanal, #sporocila').css('background', besede[1]);
      var novaBarva=document.getElementById("sporocila").style.backgroundColor;
      if(novaBarva==trenutnaBarva) sporocilo="Nova barva je neveljavna ali že enaka trenutni.";
      else sporocilo="Uspesno ste spremenili barvo.";
      break;
    case 'preimenuj':
      besede.shift();
      var besedilo2 = besede.join(' ');
      var parametri2 = besedilo2.split('\"');
      if (parametri2) {
        if(trenutniVzdevek === parametri2[1]){
          sporocilo = 'Uporabite "/vzdevek [vzdevek] za spreminjane svojega vzdevka.';
        }else{
          this.nadimek[parametri2[1]]=parametri2[3];
          sporocilo = 'Uspesno ste preimenuvali uporabnik z vzdevek: ' + parametri2[1] + ' v nadimek: ' + parametri2[3];
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};